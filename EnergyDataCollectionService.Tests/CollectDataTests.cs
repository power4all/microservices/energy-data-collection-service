using EnergyDataCollectionService.Logic.Services;
using Xunit;

namespace EnergyDataCollectionService.Tests
{
	public class CollectDataTests
	{
		[Fact]
		public void CollectData_Under140Chars_ReturnTrue()
		{
			var dataCollectionService = new DataCollectionService();

			var message = "Hello Data";
			var tweet = dataCollectionService.NewDataCollect(message);

			Assert.Equal(message, tweet.Message);
		}
	}
}