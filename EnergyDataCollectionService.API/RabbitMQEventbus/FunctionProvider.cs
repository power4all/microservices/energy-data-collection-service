﻿using EnergyDataCollectionService.Data;
using EnergyDataCollectionService.Logic.Services;
using EnergyDataCollectionService.Models.Entity.Solar.Message;
using EnergyDataCollectionService.Models.Entity.Wind.Message;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client.Events;
using RabbitMQEventbus.Extension.FunctionProvider;
using RabbitMQEventbus.Extension.Message;
using System;
using System.Collections.Generic;
using System.Text;

namespace EnergyDataCollectionService.RabbitMQEventbus
{
	public class FunctionProvider : RabbitMQFunctionProviderBase
	{
		private readonly WeatherService _weatherService;
		private readonly ILogger<FunctionProvider> _logger;
		private readonly IServiceScopeFactory _scopeFactory;

		public FunctionProvider(WeatherService weatherService, ILogger<FunctionProvider> logger, IServiceScopeFactory scopeFactory)
		{
			_weatherService = weatherService;
			_logger = logger;
			_scopeFactory = scopeFactory;
		}

		protected override Dictionary<string, Func<object, BasicDeliverEventArgs, RabbitMQMessage>> InializeFunctionsWithKeys()
		{
			var dictionary = new Dictionary<string, Func<object, BasicDeliverEventArgs, RabbitMQMessage>>();
			dictionary.Add("HandleSolarData", HandleSolarData);
			dictionary.Add("HandleWindData", HandleWindData);

			return dictionary;
		}

		private RabbitMQMessage HandleSolarData(object o, BasicDeliverEventArgs eventArgs)
		{
			var body = eventArgs.Body.ToArray();
			var message = Encoding.UTF8.GetString(body);

			try
			{
				var solarMessage = JsonConvert.DeserializeObject<SolarMessage>(message);

				using (var scope = _scopeFactory.CreateScope())
				{
					var dbcontext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

					// delete previous
					dbcontext.Database.ExecuteSqlRaw("DELETE FROM SolarData;");

					foreach (var interval in solarMessage.SolarIntervals)
					{
						dbcontext.SolarData.Add(interval);
					}
					dbcontext.SaveChanges();
				}
			}
			catch (Exception e)
			{
				_logger.LogWarning(e.StackTrace);
			}

			return null;
		}

		private RabbitMQMessage HandleWindData(object o, BasicDeliverEventArgs eventArgs)
		{
			var body = eventArgs.Body.ToArray();
			var message = Encoding.UTF8.GetString(body);

			try
			{
				var windMessage = JsonConvert.DeserializeObject<WindMessage>(message);

				using (var scope = _scopeFactory.CreateScope())
				{
					var dbcontext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

					// delete previous
					dbcontext.Database.ExecuteSqlRaw("DELETE FROM WindData;");

					foreach (var interval in windMessage.WindSpeedIntervals)
					{
						dbcontext.WindData.Add(interval);
					}

					dbcontext.SaveChanges();
				}
			}
			catch (Exception e)
			{
				_logger.LogWarning(e.StackTrace);
			}

			return null;
		}
	}
}