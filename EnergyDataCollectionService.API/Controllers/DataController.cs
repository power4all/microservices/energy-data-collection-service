using EnergyDataCollectionService.Models.Entity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RabbitMQEventbus.Extension.Eventbus;
using RabbitMQEventbus.Extension.Message;
using System.Threading.Tasks;

namespace EnergyDataCollectionService.Controllers
{
	[Route("api/[controller]")]
	[Produces("application/json")]
	[Consumes("application/json")]
	[ApiController]
	public class DataController : ControllerBase
	{
		private readonly IRabbitMQEventbus _eventbus;

		public DataController(
			IRabbitMQEventbus eventbus
			)
		{
			_eventbus = eventbus;
		}

		// GET: api/Products
		[HttpGet]
		public Task SendMessage()
		{
			var message = new WeatherData()
			{
				Message = "test",
			};

			var json = JsonConvert.SerializeObject(message);

			_eventbus.Publish(new RabbitMQMessage(new MessageDestination("WeatherExchange", "weather.data"), json));

			return Task.CompletedTask;
		}

		/*// GET: api/Products
		[HttpGet]
		public async Task<IEnumerable<dynamic>> GetData()
		{
			var serviceRoot = "https://services.odata.org/V4/TripPinServiceRW/";
			var context = new DefaultContainer(new Uri(serviceRoot));

			IEnumerable<Person> people = await context.People.ExecuteAsync();

			return people;
		}*/
	}
}