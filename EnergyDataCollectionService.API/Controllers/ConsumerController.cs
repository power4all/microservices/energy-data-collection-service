﻿using EnergyDataCollectionService.Data;
using EnergyDataCollectionService.Logic.Consumers;
using EnergyDataCollectionService.Logic.Managers;
using EnergyDataCollectionService.Logic.Services;
using EnergyDataCollectionService.Models.DTO.Consumer;
using EnergyDataCollectionService.Models.Entity.Consumers;
using EnergyDataCollectionService.Models.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RabbitMQEventbus.Extension.Eventbus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyDataCollectionService.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ConsumerController : Controller
	{
		private readonly ConsumerManager _manager;
		private readonly ApplicationDbContext _context;
		private readonly ConsumerService _consumerService;
		private readonly IRabbitMQEventbus _eventbus;
		private readonly IServiceScopeFactory _scopeFactory;

		private readonly ILogger<ConsumerController> _logger;

		public ConsumerController(ConsumerManager manager, ApplicationDbContext context, ConsumerService consumerService, IRabbitMQEventbus eventbus, IServiceScopeFactory scopeFactory)
		{
			_manager = manager;
			_context = context;
			_consumerService = consumerService;
			_eventbus = eventbus;
			_scopeFactory = scopeFactory;
		}

		//private readonly InitProducers initProducers
		[HttpGet]
		public async Task<ActionResult<IEnumerable<Consumer>>> GetConsumers()
		{
			return Ok(await _context.Consumers.Select(p => new
			{
				p.Id,
				p.ConsumerName,
				p.ConsumerType,
				p.isProducing,
				p.Attributes,
			}).ToListAsync());
		}

		[HttpPost("addconsumer")]
		public async Task<ActionResult> AddConsumer(AddConsumerRequest request)
		{
			ConsumerType type = null;
			switch (request.Type)
			{
				case "small":
					type = await _consumerService.GetOrCreateType("small");
					break;

				case "medium":
					type = await _consumerService.GetOrCreateType("medium");
					break;

				case "large":
					type = await _consumerService.GetOrCreateType("large");
					break;

				default:
					break;
			}

			await AddConsumerAsync(request, type);

			return Ok();
		}

		//[HttpPost("addprosumer")]
		//public async Task<ActionResult> AddProsumer(AddConsumerRequest request)
		//{
		//	ConsumerType type = null;
		//	switch (request.Type)
		//	{
		//		case "small":
		//			type = await _consumerService.GetOrCreateType("small");
		//			break;

		//		case "medium":
		//			type = await _consumerService.GetOrCreateType("medium");
		//			break;

		//		case "large":
		//			type = await _consumerService.GetOrCreateType("large");
		//			break;

		//		default:
		//			break;
		//	}
		//	await AddProsumerAsync(request, type);
		//	return Ok();
		//}

		private async Task AddConsumerAsync(AddConsumerRequest request, ConsumerType type)
		{
			EnergyConsumption energyConsumption;
			bool hasSolarPanels = request.Attributes == null ? false : true;
			if (request.isProducing && hasSolarPanels)
			{
				var solarPanelCount = Convert.ToInt32(request.Attributes.FirstOrDefault(a => a.AttributeName == "solarPanelCount").Value);
				energyConsumption = new EnergyConsumption(_eventbus, type, solarPanelCount, _scopeFactory)
				{
					ConsumerName = request.ConsumerName,
					isProducing = request.isProducing,
					Attributes = new List<ConsumerAttribute>(),
				};
			}
			else
			{
				energyConsumption = new EnergyConsumption(_eventbus, type, 0, _scopeFactory)
				{
					ConsumerName = request.ConsumerName,
					isProducing = request.isProducing,
					Attributes = new List<ConsumerAttribute>(),
				};
			}

			if (request.Attributes != null)
			{
				foreach (var attribute in request.Attributes)
				{
					energyConsumption.Attributes.Add(new ConsumerAttribute
					{
						AttributeName = attribute.AttributeName,
						Value = attribute.Value,
					});
				}
			}
			else
			{
				energyConsumption.Attributes.Add(new ConsumerAttribute());
			}

			await _context.Consumers.AddAsync(energyConsumption);
			await _context.SaveChangesAsync();

			_manager.Consumers.Add(energyConsumption);
		}

		private async Task AddProsumerAsync(AddConsumerRequest request, ConsumerType type)
		{
			var solarPanelCount = request.Attributes.FirstOrDefault(a => a.AttributeName == "solarPanelCount").Value;

			if (solarPanelCount == null)
			{
				throw new AttributeNotFoundException();
			}

			var energyProduction = new EnergyProduction(_eventbus, type, Convert.ToInt32(solarPanelCount), _scopeFactory)
			{
				ConsumerName = request.ConsumerName,
				isProducing = request.isProducing,
				Attributes = new List<ConsumerAttribute>(),
			};

			foreach (var attribute in request.Attributes)
			{
				energyProduction.Attributes.Add(new ConsumerAttribute
				{
					AttributeName = attribute.AttributeName,
					Value = attribute.Value,
				});
			}

			await _context.Consumers.AddAsync(energyProduction);
			await _context.SaveChangesAsync();

			_manager.Consumers.Add(energyProduction);
		}
	}
}