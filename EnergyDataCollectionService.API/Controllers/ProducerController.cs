﻿using EnergyDataCollectionService.Data;
using EnergyDataCollectionService.Logic.Managers;
using EnergyDataCollectionService.Logic.Producers;
using EnergyDataCollectionService.Logic.Services;
using EnergyDataCollectionService.Models.DTO;
using EnergyDataCollectionService.Models.Entity.Nuclear;
using EnergyDataCollectionService.Models.Entity.Producers;
using EnergyDataCollectionService.Models.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RabbitMQEventbus.Extension.Eventbus;
using Reference.TenneT;
using Reference.WebPublications.ExportServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyDataCollectionService.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ProducerController : ControllerBase
	{
		private readonly ILogger<ProducerController> _logger;

		private Container context = new Container(new Uri("https://export.webpublications.svc.tennet.org/odata"));

		private readonly ProducerManager _manager;
		private readonly ApplicationDbContext _context;
		private readonly ProducerService _producerService;
		private readonly IRabbitMQEventbus _eventbus;
		private readonly IServiceScopeFactory _scopeFactory;

		public ProducerController(ProducerManager manager, ApplicationDbContext context, ProducerService producerService, IRabbitMQEventbus eventbus, IServiceScopeFactory scopeFactory, ILogger<ProducerController> logger)
		{
			_manager = manager;
			_context = context;
			_producerService = producerService;
			_eventbus = eventbus;
			_scopeFactory = scopeFactory;
			_logger = logger;
		}

		//private readonly InitProducers initProducers

		[HttpGet("consumption")]
		public List<Load> GetConsumption()
		{
			//Uanble to use .Last since OData does not suport this mehtod
			var monthly = context.Load.Where(x => x.date > new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 3)).ToList();
			var filteredMonthly = monthly.Where(x => x.actual != null).ToList();
			bool isFirst = true;
			var i = 0;
			filteredMonthly.ForEach(x =>
			{
				if (isFirst)
				{
					isFirst = false;
				}
				else if (i < 1440)
				{
					i = i + 15;
				}
				else
				{
					i = 15;
				}
				x.date = x.date.AddMinutes(i);
			});

			return filteredMonthly;
		}

		[HttpGet("production")]
		public IEnumerable<MeasuredFeedIn> GetProduction()
		{
			//Whoops browser kan maar 1500 sets
			//Uanble to use .Last since OData does not suport this mehtod
			var monthly = context.MeasuredFeedIn.Where(x => x.date > new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 3)).ToList();
			var filteredMonthly = monthly.Where(x => x.actual != null).ToList();

			bool isFirst = true;
			var i = 0;
			filteredMonthly.ForEach(x =>
			{
				if (isFirst)
				{
					isFirst = false;
				}
				else if (i < 1440)
				{
					i = i + 15;
				}
				else
				{
					i = 15;
				}
				x.date = x.date.AddMinutes(i);
			});

			return filteredMonthly;
		}

		[HttpGet]
		public async Task<ActionResult<IEnumerable<Producer>>> GetProducers()
		{
			return Ok(await _context.Producers.Select(p => new
			{
				p.Id,
				p.ProducerName,
				p.ProducerType,
				p.Longitude,
				p.Latitude,
				p.Attributes,
			}).ToListAsync());
		}

		[HttpPost]
		public async Task<ActionResult> AddProducer(AddProducerRequest request)
		{
			switch (request.Type)
			{
				case "Solar":
					var solarType = await _producerService.GetOrCreateType("Solar");

					var solarPanelCount = request.Attributes.FirstOrDefault(a => a.AttributeName == "solarPanelCount").Value;

					if (solarPanelCount == null)
					{
						throw new AttributeNotFoundException();
					}

					var solarFarm = new SolarFarm(_eventbus, solarType, Convert.ToInt32(solarPanelCount), _scopeFactory)
					{
						ProducerName = request.ProducerName,
						Longitude = request.Longitude,
						Latitude = request.Latitude,
						Attributes = new List<ProducerAttribute>(),
					};

					foreach (var attribute in request.Attributes)
					{
						solarFarm.Attributes.Add(new ProducerAttribute
						{
							AttributeName = attribute.AttributeName,
							Value = attribute.Value,
						});
					}

					await _context.Producers.AddAsync(solarFarm);
					await _context.SaveChangesAsync();

					_manager.Producers.Add(solarFarm);
					break;

				case "Wind":
					var windType = await _producerService.GetOrCreateType("Wind");

					var windTurbineCount = request.Attributes.FirstOrDefault(a => a.AttributeName == "windTurbineCount").Value;

					if (windTurbineCount == null)
					{
						throw new AttributeNotFoundException();
					}

					var windFarm = new WindFarm(_eventbus, windType, Convert.ToInt32(windTurbineCount), _scopeFactory)
					{
						ProducerName = request.ProducerName,
						Longitude = request.Longitude,
						Latitude = request.Latitude,
						Attributes = new List<ProducerAttribute>(),
					};

					foreach (var attribute in request.Attributes)
					{
						windFarm.Attributes.Add(new ProducerAttribute
						{
							AttributeName = attribute.AttributeName,
							Value = attribute.Value,
						});
					}

					await _context.Producers.AddAsync(windFarm);
					await _context.SaveChangesAsync();

					_manager.Producers.Add(windFarm);
					break;

				case "Nuclear":
					var nuclearType = await _producerService.GetOrCreateType("Nuclear");

					var productionValue = request.Attributes.FirstOrDefault(a => a.AttributeName == "production").Value;

					if (productionValue == null)
					{
						throw new AttributeNotFoundException();
					}

					var nuclearPlant = new NuclearPlant(_eventbus, nuclearType, Convert.ToInt32(productionValue), _scopeFactory)
					{
						ProducerName = request.ProducerName,
						Longitude = request.Longitude,
						Latitude = request.Latitude,
						Attributes = new List<ProducerAttribute>(),
					};

					foreach (var attribute in request.Attributes)
					{
						nuclearPlant.Attributes.Add(new ProducerAttribute
						{
							AttributeName = attribute.AttributeName,
							Value = attribute.Value,
						});
					}

					await _context.Producers.AddAsync(nuclearPlant);
					await _context.SaveChangesAsync();

					_manager.Producers.Add(nuclearPlant);
					break;

				default:
					break;
			}

			return Ok();
		}

		[HttpDelete]
		public async Task<ActionResult> DeleteProducer([FromQuery] long producerId)
		{
			var producer = await _context.Producers.Include(p => p.Attributes).Where(x => x.Id == producerId).FirstOrDefaultAsync();

			if (producer == null)
			{
				return NotFound();
			}

			_context.Producers.Remove(producer);
			await _context.SaveChangesAsync();

			// get index to remove
			List<Producer> producers = _manager.Producers.Cast<Producer>().ToList();

			var index = producers.Select((prod, index) => (prod, index)).FirstOrDefault(x => x.prod.Id == producer.Id).index;

			_manager.Producers.RemoveAt(index);

			return Ok();
		}

		[HttpPost("adjust")]
		public IActionResult Adjust([FromBody] AdjustEnergyModel model)
		{
			if (!ModelState.IsValid) return BadRequest(model);

			_manager.SetNuclearEnergy(model.Id, model.Value);
			return Ok();
		}
	}
}