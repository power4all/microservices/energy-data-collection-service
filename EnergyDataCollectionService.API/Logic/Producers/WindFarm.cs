﻿using EnergyDataCollectionService.Data;
using EnergyDataCollectionService.Models.Entity.Producers;
using EnergyDataCollectionService.Models.Exceptions;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQEventbus.Extension.Eventbus;
using RabbitMQEventbus.Extension.Message;
using System;
using System.Linq;
using System.Timers;

namespace EnergyDataCollectionService.Logic.Producers
{
	public class WindFarm : Producer, IProducer
	{
		private readonly IRabbitMQEventbus _eventbus;
		private readonly IServiceScopeFactory _scopeFactory;
		private int _windTurbineCount;

		public WindFarm(
			IRabbitMQEventbus eventbus,
			ProducerType producerType,
			int windTurbineCount,
			IServiceScopeFactory scopeFactory
		)
		{
			_eventbus = eventbus;
			ProducerType = producerType;
			_scopeFactory = scopeFactory;
			_windTurbineCount = windTurbineCount;

			InitTimer();
		}

		public long Output => GetCurrentWindEnergy();

		private void InitTimer()
		{
			Timer t = new Timer();
			t.Interval = 60000; // In milliseconds
			t.AutoReset = true; // False stops it from repeating
			t.Elapsed += new ElapsedEventHandler(CheckEnergyAsync);
			t.Start();
		}

		private void CheckEnergyAsync(object sender, ElapsedEventArgs e)
		{
			try
			{
				var output = Convert.ToInt64(GetCurrentWindEnergy());

				var message = new ProducerMessage
				{
					Output = output,
					ProducerId = Id,
					ProducerType = ProducerType.Type
				};

				var json = JsonConvert.SerializeObject(message);

				_eventbus.Publish(new RabbitMQMessage(new MessageDestination("EnergyExchange", "energy.producers.wind"), json));
			}
			catch (NoDataException ex)
			{
				Console.WriteLine("WindFarm: " + ex.Message);
			}
		}

		private int GetCurrentWindEnergy()
		{
			// Vermogen         = π / 2 * r^2 * v^3 * p * η
			// Radius           = de lengte van de wieken van de windmolen (
			// AirDensity       = de dichtheid van de lucht, deze wordt beïnvloed door de tempratuur  (kg/m3 )
			// EffiencyFactor   = de efficienty van de turbine, deze kan maximaal 59,26. Dit is afhankelijk van de richting van wind in tegenstelling to de wieken van de motor.

			double radius = Math.Pow(58, 2);
			double efficiencyFactor = 0.4;
			double windSpeed;
			double temperature;// Niet relevant voor nu, alleen wanneer we accurater willen gaan rekenen
			double airDensity = 1.2;

			// retrieve latest GHI value
			using (var scope = _scopeFactory.CreateScope())
			{
				var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

				Random r = new Random();
				var windData = context.WindData.ToList();
				var oldestWindData = windData[r.Next(1, windData.Count)];

				if (oldestWindData != null)
				{
					windSpeed = Math.Pow(oldestWindData.WindSpeed, 3);
					temperature = oldestWindData.Temprature;

					var currentTotalWindPower = (int)((Math.PI / 2) / 1000 * radius * windSpeed * airDensity * efficiencyFactor * _windTurbineCount);

					return currentTotalWindPower;
				}

				throw new NoDataException();
			}
		}
	}
}