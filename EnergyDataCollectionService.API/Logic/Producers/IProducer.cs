using EnergyDataCollectionService.Models.Entity.Producers;

namespace EnergyDataCollectionService.Logic.Producers
{
	public interface IProducer
	{
		long Output { get; }
		ProducerType ProducerType { get; }
	}
}