using EnergyDataCollectionService.Data;
using EnergyDataCollectionService.Logic.Managers;
using EnergyDataCollectionService.Logic.Services;
using EnergyDataCollectionService.Models.Entity.Producers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQEventbus.Extension.Eventbus;
using System;
using System.Linq;

namespace EnergyDataCollectionService.Logic.Producers
{
	public class InitProducers
	{
		private readonly ProducerService _producerService;
		private readonly ProducerManager _producerManager;
		private readonly IRabbitMQEventbus _eventbus;
		private readonly ApplicationDbContext _context;
		private readonly IServiceScopeFactory _scopeFactory;

		public InitProducers(
			ProducerService producerService,
			ProducerManager producerManager,
			IRabbitMQEventbus eventbus,
			ApplicationDbContext context,
			IServiceScopeFactory scopeFactory
		)
		{
			_producerService = producerService;
			_producerManager = producerManager;
			_eventbus = eventbus;
			_context = context;
			_scopeFactory = scopeFactory;

			CreateProducers();
		}

		private void CreateProducers()
		{
			foreach (var producer in _context.Producers.Where(x => x.ProducerType.Type == "Wind").Include(x => x.Attributes))
			{
				CreateWindFarm(producer);
			}

			foreach (var producer in _context.Producers.Where(x => x.ProducerType.Type == "Solar").Include(x => x.Attributes))
			{
				CreateSolarFarm(producer);
			}

			foreach (var producer in _context.Producers.Where(x => x.ProducerType.Type == "Nuclear").Include(x => x.Attributes))
			{
				CreateNuclearPlant(producer);
			}
		}

		private void CreateNuclearPlant(Producer producer)
		{
			var type = _producerService.GetOrCreateType("Nuclear").Result;

			var productionValue = Convert.ToInt32(producer.Attributes.FirstOrDefault(x => x.AttributeName == "production").Value);

			var nuclearPlant = new NuclearPlant(_eventbus, type, productionValue, _scopeFactory);
			_producerManager.Producers.Add(nuclearPlant);
		}

		private void CreateSolarFarm(Producer producer)
		{
			var type = _producerService.GetOrCreateType("Solar").Result;

			int solarPanelCount = Convert.ToInt32(producer.Attributes.FirstOrDefault(a => a.AttributeName == "solarPanelCount").Value);

			var solarFarm = new SolarFarm(_eventbus, type, solarPanelCount, _scopeFactory);
			_producerManager.Producers.Add(solarFarm);
		}

		private void CreateWindFarm(Producer producer)
		{
			var type = _producerService.GetOrCreateType("Wind").Result;

			var windTurbinesCount = Convert.ToInt32(producer.Attributes.FirstOrDefault(x => x.AttributeName == "windTurbineCount").Value);

			var windFarm = new WindFarm(_eventbus, type, windTurbinesCount, _scopeFactory); // 437 is het aantal windmolens in West-Nederland
			_producerManager.Producers.Add(windFarm);
		}

		private void ClearDb()
		{
			_context.Database.ExecuteSqlRaw("truncate table ProducerMessages;");
			_context.Database.ExecuteSqlRaw("delete from Producers;");
			_context.Database.ExecuteSqlRaw("ALTER TABLE Producers AUTO_INCREMENT=1;");
			_context.GetDependencies().StateManager.ResetState();

			Console.WriteLine("Cleared DB");
		}

		public void LogStarted()
		{
			Console.WriteLine("Started");
		}
	}
}