using EnergyDataCollectionService.Models.Entity.Producers;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQEventbus.Extension.Eventbus;
using RabbitMQEventbus.Extension.Message;
using System;
using System.Timers;

namespace EnergyDataCollectionService.Logic.Producers
{
	public class NuclearPlant : Producer, IProducer
	{
		private readonly IRabbitMQEventbus _eventbus;
		private readonly IServiceScopeFactory _scopeFactory;
		private readonly Random _random;

		public NuclearPlant(IRabbitMQEventbus eventbus, ProducerType producerType, int initialValue, IServiceScopeFactory scopeFactory)
		{
			_eventbus = eventbus;
			_random = new Random();
			_scopeFactory = scopeFactory;
			ProducerType = producerType;
			Output = initialValue;
			InitTimer();
		}

		// 3.300.000 = 93%
		private readonly long maxValue = 3548387;

		public long Output { get; set; }

		private void InitTimer()
		{
			Timer t = new Timer();
			t.Interval = 3000; // In milliseconds
			t.AutoReset = true; // False stops it from repeating
			t.Elapsed += new ElapsedEventHandler(CheckEnergy);
			t.Start();
		}

		private void CheckEnergy(object sender, ElapsedEventArgs e)
		{
			var message = new ProducerMessage
			{
				Output = Output,
				ProducerId = Id,
				ProducerType = ProducerType.Type
			};

			var json = JsonConvert.SerializeObject(message);

			_eventbus.Publish(new RabbitMQMessage(new MessageDestination("EnergyExchange", "energy.producers.nuclear"), json));
		}

		public void Adjust(long value)
		{
			Output = value < maxValue ? value : Output;
		}
	}
}