using EnergyDataCollectionService.Data;
using EnergyDataCollectionService.Models.Entity.Producers;
using EnergyDataCollectionService.Models.Exceptions;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQEventbus.Extension.Eventbus;
using RabbitMQEventbus.Extension.Message;
using System;
using System.Linq;
using System.Timers;

namespace EnergyDataCollectionService.Logic.Producers
{
	public class SolarFarm : Producer, IProducer
	{
		private readonly IRabbitMQEventbus _eventbus;
		private readonly IServiceScopeFactory _scopeFactory;
		private readonly Random _random;
		private int _solarPanelCount;

		public SolarFarm(IRabbitMQEventbus eventbus, ProducerType producerType, int solarPanelCount, IServiceScopeFactory scopeFactory)
		{
			_eventbus = eventbus;
			_random = new Random();
			_scopeFactory = scopeFactory;
			ProducerType = producerType;

			_solarPanelCount = solarPanelCount;
			InitTimer();
		}

		public long Output => GetCurrentSolarEnergy();

		private void InitTimer()
		{
			Timer t = new Timer();
			t.Interval = 60000; // In milliseconds
			t.AutoReset = true; // False stops it from repeating
			t.Elapsed += new ElapsedEventHandler(CheckEnergyAsync);
			t.Start();
		}

		private async void CheckEnergyAsync(object sender, ElapsedEventArgs e)
		{
			try
			{
				var output = Convert.ToInt64(GetCurrentSolarEnergy());

				var message = new ProducerMessage
				{
					Output = output,
					ProducerId = Id,
					ProducerType = ProducerType.Type
				};

				var json = JsonConvert.SerializeObject(message);

				_eventbus.Publish(new RabbitMQMessage(new MessageDestination("EnergyExchange", "energy.producers.solar"), json));
			}
			catch (NoDataException ex)
			{
				Console.WriteLine("SolarFarm: " + ex.Message);
			}
		}

		public long GetCurrentSolarEnergy()
		{
			const double nominalPower = 300; // wattpiek
			const double solarPanelArea = 1.65 * 1.00; // m²
			const double efficiency = 0.192;

			double totalSolarPanelArea = solarPanelArea * _solarPanelCount;

			// retrieve latest GHI value
			using (var scope = _scopeFactory.CreateScope())
			{
				var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

				Random r = new Random();
				var solarData = context.SolarData.ToList();
				var oldestSolarData = solarData[r.Next(1, solarData.Count)];

				if (oldestSolarData != null)
				{
					var solarGHI = oldestSolarData.GHI;

					if (solarGHI == 0)
					{
						return 0;
					}

					double currentTotalSolarPower = totalSolarPanelArea * solarGHI * efficiency;

					return (long)currentTotalSolarPower / 1000; // in kilowatt
				}

				throw new NoDataException();
			}
		}
	}
}