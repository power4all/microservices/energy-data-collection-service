﻿using EnergyDataCollectionService.Data;
using EnergyDataCollectionService.Logic.Managers;
using EnergyDataCollectionService.Logic.Services;
using EnergyDataCollectionService.Models.Entity.Consumers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQEventbus.Extension.Eventbus;
using System;
using System.Linq;

namespace EnergyDataCollectionService.Logic.Consumers
{
	public class InitConsumers
	{
		private readonly ConsumerService _consumerService;
		private readonly ConsumerManager _consumerManager;
		private readonly IRabbitMQEventbus _eventbus;
		private readonly ApplicationDbContext _context;
		private readonly IServiceScopeFactory _scopeFactory;

		public InitConsumers(
			ConsumerService consumerService,
			ConsumerManager consumerManager,
			IRabbitMQEventbus eventbus,
			ApplicationDbContext context,
			IServiceScopeFactory scopeFactory
		)
		{
			_consumerService = consumerService;
			_consumerManager = consumerManager;
			_eventbus = eventbus;
			_context = context;
			_scopeFactory = scopeFactory;

			CreateConsumers();
		}

		private void CreateConsumers()
		{
			foreach (var consumer in _context.Consumers)
			{
				CreateConsumption(consumer);
			}
		}

		private void CreateConsumption(Consumer consumer)
		{
			var type = _consumerService.GetOrCreateType("").Result;
			EnergyConsumption energyConsumption = null;
			bool hasSolarPanels = consumer.Attributes == null ? false : true;
			if (consumer.isProducing && hasSolarPanels)
			{
				int solarPanelCount = Convert.ToInt32(consumer.Attributes.FirstOrDefault(a => a.AttributeName == "solarPanelCount").Value);
				energyConsumption = new EnergyConsumption(_eventbus, type, solarPanelCount, _scopeFactory);
			}
			else
			{
				energyConsumption = new EnergyConsumption(_eventbus, type, 0, _scopeFactory);
			}
			_consumerManager.Consumers.Add(energyConsumption);
		}

		private void CreateProduction(Consumer consumer, ConsumerType type)
		{
			int solarPanelCount = Convert.ToInt32(consumer.Attributes.FirstOrDefault(a => a.AttributeName == "solarPanelCount").Value);
			var energyProduction = new EnergyProduction(_eventbus, type, solarPanelCount, _scopeFactory);
			_consumerManager.Consumers.Add(energyProduction);
		}

		private void ClearDb()
		{
			_context.Database.ExecuteSqlRaw("truncate table ConsumerMessages;");
			_context.Database.ExecuteSqlRaw("delete from Consumers;");
			_context.Database.ExecuteSqlRaw("ALTER TABLE Consumers AUTO_INCREMENT=1;");
			_context.GetDependencies().StateManager.ResetState();

			Console.WriteLine("Cleared DB");
		}

		public void LogStarted()
		{
			Console.WriteLine("Started");
		}
	}
}