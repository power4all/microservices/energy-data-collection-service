﻿using EnergyDataCollectionService.Logic.Services;
using EnergyDataCollectionService.Models.Entity.Consumers;
using EnergyDataCollectionService.Models.Exceptions;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQEventbus.Extension.Eventbus;
using RabbitMQEventbus.Extension.Message;
using System;
using System.Timers;

namespace EnergyDataCollectionService.Logic.Consumers
{
	public class EnergyProduction : Consumer, IConsumer
	{
		private readonly IRabbitMQEventbus _eventbus;
		private readonly IServiceScopeFactory _scopeFactory;
		private readonly Random _random;
		private int _solarPanelCount;

		public EnergyProduction(IRabbitMQEventbus eventbus, ConsumerType consumerType, int solarPanelCount, IServiceScopeFactory scopeFactory)
		{
			_eventbus = eventbus;
			_random = new Random();
			_scopeFactory = scopeFactory;
			ConsumerType = consumerType;
			_solarPanelCount = solarPanelCount;
			InitTimer();
		}

		public long Production => SolarService.GetCurrentSolarEnergy(_scopeFactory, _solarPanelCount);

		public long Consumption => EnergyConsumptionService.GetCurrentConsumption(ConsumerType, _scopeFactory);

		private void InitTimer()
		{
			Timer t = new Timer();
			t.Interval = 60000; // In milliseconds
			t.AutoReset = true; // False stops it from repeating
			t.Elapsed += new ElapsedEventHandler(checkProductionAsync);
			t.Start();
		}

		private void checkProductionAsync(object sender, ElapsedEventArgs e)
		{
			try
			{
				var production = Convert.ToInt64(SolarService.GetCurrentSolarEnergy(_scopeFactory, _solarPanelCount));

				var message = new ConsumerMessage
				{
					Production = production,
					ConsumerId = Id,
					ConsumerType = ConsumerType.Type
				};

				var json = JsonConvert.SerializeObject(message);

				_eventbus.Publish(new RabbitMQMessage(new MessageDestination("EnergyExchange", "energy.consumers.consumption"), json));
			}
			catch (NoDataException ex)
			{
				Console.WriteLine("Consumer_Consumption: " + ex.Message);
			}
		}
	}
}