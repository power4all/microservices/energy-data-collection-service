using EnergyDataCollectionService.Models.Entity.Consumers;

namespace EnergyDataCollectionService.Logic.Consumers
{
	public interface IConsumer
	{
		long Id { get; }
		long Consumption { get; }
		long Production { get; }
		ConsumerType ConsumerType { get; }
		bool isProducing { get; }
	}
}