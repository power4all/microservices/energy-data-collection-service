using EnergyDataCollectionService.Data;
using EnergyDataCollectionService.Models.Entity.Producers;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyDataCollectionService.Logic.Services
{
	public class ProducerService
	{
		private readonly ApplicationDbContext _context;

		public ProducerService(
			ApplicationDbContext context
		)
		{
			_context = context;
		}

		public async Task<ProducerType> GetOrCreateType(string type)
		{
			var existing = _context.ProducerTypes.FirstOrDefault(e => e.Type.Contains(type));

			if (existing != null)
			{
				return existing;
			}

			var producerType = new ProducerType
			{
				Type = type
			};
			await _context.ProducerTypes.AddAsync(producerType);
			await _context.SaveChangesAsync();

			return producerType;
		}
	}
}