﻿using EnergyDataCollectionService.Data;
using EnergyDataCollectionService.Models.Entity.Consumers;
using EnergyDataCollectionService.Models.Exceptions;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace EnergyDataCollectionService.Logic.Services
{
	public static class EnergyConsumptionService
	{
		public static long GetCurrentConsumption(ConsumerType consumerType, IServiceScopeFactory _scopeFactory)
		{
			Random _random = new Random();
			using (var scope = _scopeFactory.CreateScope())
			{
				var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

				int min = 0;
				int max = 0;

				switch (consumerType.Type)
				{
					case "small":
						min = 1640;
						max = 1730;
						break;

					case "medium":
						min = 2050;
						max = 3290;
						break;

					case "large":
						min = 3950;
						max = 4490;
						break;
				}
				return _random.Next(min, max);
			}
			throw new NoDataException();
		}
	}
}