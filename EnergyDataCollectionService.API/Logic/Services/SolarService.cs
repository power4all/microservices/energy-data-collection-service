﻿using EnergyDataCollectionService.Data;
using EnergyDataCollectionService.Models.Exceptions;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace EnergyDataCollectionService.Logic.Services
{
	public static class SolarService
	{
		public static long GetCurrentSolarEnergy(IServiceScopeFactory _scopeFactory, int _solarPanelCount)
		{
			const double nominalPower = 300; // wattpiek

			double solarGHI = 0;

			// retrieve latest GHI value
			using (var scope = _scopeFactory.CreateScope())
			{
				var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

				Random r = new Random();

				var solarData = context.SolarData.ToList();
				if (solarData.Count == 0)
				{
					return 0;
				}
				var oldestSolarData = solarData[r.Next(1, solarData.Count)];
				if (oldestSolarData != null)
				{
					solarGHI = oldestSolarData.GHI;

					if (solarGHI == 0)
					{
						return 0;
					}

					double GHIFactor = solarGHI / 1000;

					double currentTotalSolarPower = nominalPower * GHIFactor * _solarPanelCount;

					return (long)currentTotalSolarPower / 1000;
				}

				throw new NoDataException();
			}
		}
	}
}