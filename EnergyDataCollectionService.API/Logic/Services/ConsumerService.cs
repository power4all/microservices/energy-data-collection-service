using EnergyDataCollectionService.Data;
using EnergyDataCollectionService.Models.Entity.Consumers;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace EnergyDataCollectionService.Logic.Services
{
	public class ConsumerService
	{
		private readonly ApplicationDbContext _context;

		public ConsumerService(ApplicationDbContext context)
		{
			_context = context;
		}

		public async Task<ConsumerType> GetOrCreateType(string type)
		{
			var existing = _context.ConsumerTypes.FirstOrDefault(e => e.Type.Contains(type));

			if (existing != null)
			{
				return existing;
			}

			if ((type != "small") && (type != "medium") && (type != "large"))
			{
				type = CreateRandomType();
			}

			var consumerType = new ConsumerType
			{
				Type = type
			};
			await _context.ConsumerTypes.AddAsync(consumerType);
			await _context.SaveChangesAsync();

			return consumerType;
		}

		private string CreateRandomType()
		{
			Random r = new Random();
			var typeNumber = r.Next(3);
			if (typeNumber == 0)
			{
				return "small";
			}
			else if (typeNumber == 1)
			{
				return "medium";
			}
			else
			{
				return "large";
			}
		}
	}
}