using EnergyDataCollectionService.Models.Entity;

namespace EnergyDataCollectionService.Logic.Services
{
	public class WeatherService
	{
		public WeatherData WeatherData { get; set; }

		public WeatherService()
		{
			WeatherData = new WeatherData
			{
				Message = "Empty Weather",
			};
		}
	}
}