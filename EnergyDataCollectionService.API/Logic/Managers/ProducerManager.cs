﻿using EnergyDataCollectionService.Logic.Producers;
using EnergyDataCollectionService.Models.DTO;
using Newtonsoft.Json;
using RabbitMQEventbus.Extension.Eventbus;
using RabbitMQEventbus.Extension.Message;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

namespace EnergyDataCollectionService.Logic.Managers
{
	public class ProducerManager
	{
		private readonly IRabbitMQEventbus _eventbus;
		public List<IProducer> Producers { get; set; }

		public ProducerManager(
			IRabbitMQEventbus eventbus
		)
		{
			_eventbus = eventbus;
			Producers = new List<IProducer>();
			InitTimer();
		}

		private void InitTimer()
		{
			Timer t = new Timer();
			t.Interval = 30000; // In milliseconds
			t.AutoReset = true; // False stops it from repeating
			t.Elapsed += new ElapsedEventHandler(SendTotalEnergyProduction);
			t.Elapsed += new ElapsedEventHandler(SendTotalNuclearProduction);
			t.Elapsed += new ElapsedEventHandler(SendTotalSolarProduction);
			t.Elapsed += new ElapsedEventHandler(SendTotalWindProduction);
			t.Start();
		}

		private void SendTotalEnergyProduction(object sender, ElapsedEventArgs elapsedEventArgs)
		{
			var message = new ProducerTotalEnergyMessage
			{
				TotalOutput = GetTotalEnergyProduction(Producers),
				ProducerCount = Producers.Count,
			};

			var json = JsonConvert.SerializeObject(message);

			_eventbus.Publish(new RabbitMQMessage(new MessageDestination("EnergyExchange", "energy.producers.total"), json));
		}

		private void SendTotalNuclearProduction(object sender, ElapsedEventArgs elapsedEventArgs)
		{
			var producers = Producers.FindAll(p => p.ProducerType.Type.Equals("Nuclear"));

			var message = new ProducerSubtotalEnergyMessage
			{
				TotalOutput = GetTotalEnergyProduction(producers),
				ProducerType = "Nuclear",
				ProducerCount = producers.Count,
			};

			var json = JsonConvert.SerializeObject(message);

			_eventbus.Publish(new RabbitMQMessage(new MessageDestination("EnergyExchange", "energy.producers.total.nuclear"), json));
		}

		private void SendTotalSolarProduction(object sender, ElapsedEventArgs elapsedEventArgs)
		{
			var producers = Producers.FindAll(p => p.ProducerType.Type.Equals("Solar"));

			var message = new ProducerSubtotalEnergyMessage
			{
				TotalOutput = GetTotalEnergyProduction(producers),
				ProducerType = "Solar",
				ProducerCount = producers.Count,
			};

			var json = JsonConvert.SerializeObject(message);

			_eventbus.Publish(new RabbitMQMessage(new MessageDestination("EnergyExchange", "energy.producers.total.solar"), json));
		}

		private void SendTotalWindProduction(object sender, ElapsedEventArgs elapsedEventArgs)
		{
			var producers = Producers.FindAll(p => p.ProducerType.Type.Equals("Wind"));

			var message = new ProducerSubtotalEnergyMessage
			{
				TotalOutput = GetTotalEnergyProduction(producers),
				ProducerType = "Wind",
				ProducerCount = producers.Count,
			};

			var json = JsonConvert.SerializeObject(message);

			_eventbus.Publish(new RabbitMQMessage(new MessageDestination("EnergyExchange", "energy.producers.total.wind"), json));
		}

		private long GetTotalEnergyProduction(List<IProducer> producers)
		{
			long totalEnergy = 0;
			foreach (var producer in producers)
			{
				var energy = producer.Output;
				totalEnergy += energy;
			}

			return totalEnergy;
		}

		public void SetNuclearEnergy(long id, long value)
		{
			List<IProducer> producers = Producers.FindAll(x => x.ProducerType.Equals("Nuclear"));
			List<NuclearPlant> nuclearPlants = producers.Cast<NuclearPlant>().ToList();
			//todo adjust with id
			nuclearPlants.ForEach(x => x.Adjust(value));
		}
	}
}