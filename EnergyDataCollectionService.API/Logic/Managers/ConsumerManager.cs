using EnergyDataCollectionService.Logic.Consumers;
using EnergyDataCollectionService.Models.DTO;
using EnergyDataCollectionService.Models.DTO.Consumer;
using Newtonsoft.Json;
using RabbitMQEventbus.Extension.Eventbus;
using RabbitMQEventbus.Extension.Message;
using System.Collections.Generic;
using System.Timers;

namespace EnergyDataCollectionService.Logic.Managers
{
	public class ConsumerManager
	{
		private readonly IRabbitMQEventbus _eventbus;
		public List<IConsumer> Consumers { get; set; }

		public ConsumerManager(
			IRabbitMQEventbus eventbus
		)
		{
			_eventbus = eventbus;
			Consumers = new List<IConsumer>();
			InitTimer();
		}

		private void InitTimer()
		{
			Timer t = new Timer();
			t.Interval = 30000; // In milliseconds
			t.AutoReset = true; // False stops it from repeating
			t.Elapsed += new ElapsedEventHandler(SendTotalEnergy);
			t.Elapsed += new ElapsedEventHandler(SendSubtotalEnergyConsumption);
			t.Elapsed += new ElapsedEventHandler(SendIndividualConsumers);
			t.Start();
		}

		private void SendTotalEnergy(object sender, ElapsedEventArgs elapsedEventArgs)
		{
			var message = new ConsumerTotalEnergyMessage
			{
				TotalConsumption = GetTotalEnergyConsumption(Consumers),
				TotalProduction = GetTotalEnergyProduction(Consumers),
				ConsumerCount = Consumers.Count,
			};

			var json = JsonConvert.SerializeObject(message);

			_eventbus.Publish(new RabbitMQMessage(new MessageDestination("EnergyExchange", "energy.consumers.total"), json));
		}

		private void SendTotalEnergyProduction(object sender, ElapsedEventArgs elapsedEventArgs)
		{
			var message = new ConsumerTotalEnergyMessage
			{
				TotalConsumption = GetTotalEnergyConsumption(Consumers),
				TotalProduction = GetTotalEnergyProduction(Consumers),
				ConsumerCount = Consumers.Count,
			};

			var json = JsonConvert.SerializeObject(message);

			_eventbus.Publish(new RabbitMQMessage(new MessageDestination("EnergyExchange", "energy.consumers.total.production"), json));
		}

		private void SendIndividualConsumers(object sender, ElapsedEventArgs elapsedEventArgs)
		{
			foreach (var consumer in Consumers)
			{
				var message = new ConsumerMessage
				{
					Production = consumer.Production,
					Consumption = consumer.Consumption,
					ConsumerType = consumer.ConsumerType.Type,
					ConsumerId = consumer.Id + 1
				};
				var json = JsonConvert.SerializeObject(message);
				_eventbus.Publish(new RabbitMQMessage(new MessageDestination("EnergyExchange", "energy.consumer"), json));
			}
		}

		private void SendSubtotalEnergyConsumption(object sender, ElapsedEventArgs elapsedEventArgs)
		{
			SendConsumers("small");
			SendConsumers("medium");
			SendConsumers("large");
		}

		private long GetTotalEnergyConsumption(List<IConsumer> consumers)
		{
			long totalEnergy = 0;
			foreach (var consumer in consumers)
			{
				var energy = consumer.Consumption;
				totalEnergy += energy;
			}

			return totalEnergy;
		}

		private long GetTotalEnergyProduction(List<IConsumer> consumers)
		{
			long totalEnergy = 0;
			foreach (var consumer in consumers)
			{
				var energy = consumer.Production;
				totalEnergy += energy;
			}

			return totalEnergy;
		}

		private void SendConsumers(string type)
		{
			var consumers = Consumers.FindAll(p => p.ConsumerType.Type.Equals(type));

			var message = new ConsumerSubtotalEnergyMessage
			{
				TotalConsumption = GetTotalEnergyConsumption(consumers),
				TotalProduction = GetTotalEnergyProduction(consumers),
				ConsumerCount = consumers.Count,
				ConsumerType = type
			};

			var json = JsonConvert.SerializeObject(message);

			_eventbus.Publish(new RabbitMQMessage(new MessageDestination("EnergyExchange", "energy.consumers.total." + type), json));
		}
	}
}