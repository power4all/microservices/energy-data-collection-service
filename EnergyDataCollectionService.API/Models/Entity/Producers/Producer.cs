using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyDataCollectionService.Models.Entity.Producers
{
	public class Producer
	{
		public Producer(ProducerType producerType)
		{
			ProducerType = producerType;
		}

		public Producer()
		{
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }

		public string ProducerName { get; set; }
		public string Longitude { get; set; }
		public string Latitude { get; set; }
		public virtual List<ProducerAttribute> Attributes { get; set; }

		public long ProducerTypeId { get; set; }
		public ProducerType ProducerType { get; set; }
	}
}