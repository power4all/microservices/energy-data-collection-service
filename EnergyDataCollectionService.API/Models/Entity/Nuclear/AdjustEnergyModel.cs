﻿namespace EnergyDataCollectionService.Models.Entity.Nuclear
{
	public class AdjustEnergyModel
	{
		public long Id { get; set; }
		public long Value { get; set; }
	}
}