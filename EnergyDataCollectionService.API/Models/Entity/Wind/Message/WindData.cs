﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyDataCollectionService.Models.Entity.Wind.Message
{
	public class WindData
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; init; }

		public double WindSpeed { get; init; }
		public double Temprature { get; init; }
		public DateTime Date { get; init; }
	}
}