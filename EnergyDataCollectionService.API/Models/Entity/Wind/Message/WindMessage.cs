﻿using System;
using System.Collections.Generic;

namespace EnergyDataCollectionService.Models.Entity.Wind.Message
{
	public class WindMessage
	{
		public List<WindData> WindSpeedIntervals { get; init; }
		public DateTime StartTime { get; init; }
		public DateTime EndTime { get; init; }
	}
}