﻿using System.Collections.Generic;

namespace EnergyDataCollectionService.Models.Entity.Wind
{
	public class Data
	{
		public List<Timeline> Timelines { get; init; }
	}
}