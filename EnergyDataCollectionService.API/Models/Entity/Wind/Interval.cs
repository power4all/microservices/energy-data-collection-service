﻿using System;

namespace EnergyDataCollectionService.Models.Entity.Wind
{
	public class Interval
	{
		public DateTime StartTime { get; init; }
		public Values Values { get; init; }
	}
}