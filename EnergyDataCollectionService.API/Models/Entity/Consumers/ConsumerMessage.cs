using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace EnergyDataCollectionService.Models.Entity.Consumers
{
	public class ConsumerMessage
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }

		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime DateTime { get; set; }

		public double Production { get; set; }

		public double Consumption { get; set; }
		public string ConsumerType { get; set; }

		public long ConsumerId { get; set; }

		[JsonIgnore]
		[IgnoreDataMember]
		public virtual Consumer Consumer { get; set; }
	}
}