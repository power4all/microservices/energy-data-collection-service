using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyDataCollectionService.Models.Entity.Consumers
{
	public class Consumer
	{
		public Consumer(ConsumerType consumerType)
		{
			ConsumerType = consumerType;
		}

		public Consumer()
		{
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }

		public string ConsumerName { get; set; }
		public virtual List<ConsumerAttribute> Attributes { get; set; }

		public long ConsumerTypeId { get; set; }
		public ConsumerType ConsumerType { get; set; }
		public bool isProducing { get; set; }
	}
}