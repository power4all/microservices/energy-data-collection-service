﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyDataCollectionService.Models.Entity.Consumers
{
	public class ConsumerAttribute
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }

		public string AttributeName { get; set; } = "solarPanelCount";
		public string Value { get; set; } = "0";
	}
}