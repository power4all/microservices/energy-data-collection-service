﻿using System;
using System.Collections.Generic;

namespace EnergyDataCollectionService.Models.Entity.Solar.Message
{
	public class SolarMessage
	{
		public List<SolarData> SolarIntervals { get; init; }
		public DateTime StartTime { get; init; }
		public DateTime EndTime { get; init; }
	}
}