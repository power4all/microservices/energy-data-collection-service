using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EnergyDataCollectionService.Models.Entity.Solar.Message
{
	public class SolarData
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; init; }

		public double GHI { get; init; }
		public DateTime Date { get; init; }
	}
}