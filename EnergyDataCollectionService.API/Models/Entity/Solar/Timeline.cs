﻿using System;
using System.Collections.Generic;

namespace EnergyDataCollectionService.Models.Entity.Solar
{
	public class Timeline
	{
		public string Timestep { get; init; }
		public DateTime StartTime { get; init; }
		public DateTime EndTime { get; init; }
		public List<Interval> Intervals { get; init; }
	}
}