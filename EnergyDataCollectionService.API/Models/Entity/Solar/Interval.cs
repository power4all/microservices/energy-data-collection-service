﻿using System;

namespace EnergyDataCollectionService.Models.Entity.Solar
{
	public class Interval
	{
		public DateTime StartTime { get; init; }
		public Values Values { get; init; }
	}
}