﻿using System.Collections.Generic;

namespace EnergyDataCollectionService.Models.Entity.Solar
{
	public class Data
	{
		public List<Timeline> Timelines { get; init; }
	}
}