namespace EnergyDataCollectionService.Models.Entity
{
	public class WeatherData
	{
		public long Id { get; set; }
		public string Message { get; set; }
	}
}