﻿namespace EnergyDataCollectionService.Models.DTO
{
	public class ProducerTotalEnergyMessage
	{
		public long TotalOutput { get; set; }
		public long ProducerCount { get; set; }
	}
}