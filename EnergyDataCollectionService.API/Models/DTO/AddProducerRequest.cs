﻿using System.Collections.Generic;

namespace EnergyDataCollectionService.Models.DTO
{
	public class AddProducerRequest
	{
		public string ProducerName { get; set; }
		public string Type { get; set; }
		public string Longitude { get; set; }
		public string Latitude { get; set; }
		public List<ProducerAttributeDTO> Attributes { get; set; }
	}
}