﻿namespace EnergyDataCollectionService.Models.DTO
{
	public class ConsumerTotalEnergyProductionMessage
	{
		public long TotalOutput { get; set; }
		public long ConsumerCount { get; set; }
	}
}