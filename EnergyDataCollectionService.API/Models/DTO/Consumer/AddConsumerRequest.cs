﻿using System.Collections.Generic;

namespace EnergyDataCollectionService.Models.DTO.Consumer
{
	public class AddConsumerRequest
	{
		public string ConsumerName { get; set; }
		public string Type { get; set; }
		public bool isProducing { get; set; }
		public List<ConsumerAttributeDTO> Attributes { get; set; }
	}
}