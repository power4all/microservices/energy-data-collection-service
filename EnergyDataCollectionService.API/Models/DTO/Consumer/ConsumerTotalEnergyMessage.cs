﻿namespace EnergyDataCollectionService.Models.DTO
{
	public class ConsumerTotalEnergyMessage
	{
		public long TotalConsumption { get; set; }
		public long TotalProduction { get; set; }
		public long ConsumerCount { get; set; }
	}
}