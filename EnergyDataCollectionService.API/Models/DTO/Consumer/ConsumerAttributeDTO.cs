﻿namespace EnergyDataCollectionService.Models.DTO.Consumer
{
	public class ConsumerAttributeDTO
	{
		public string AttributeName { get; set; } = "solarPanelCount";
		public string Value { get; set; } = "0";
	}
}