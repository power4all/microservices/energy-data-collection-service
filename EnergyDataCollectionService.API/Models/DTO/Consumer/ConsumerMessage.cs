﻿using System;

namespace EnergyDataCollectionService.Models.DTO.Consumer
{
	public class ConsumerMessage
	{
		public long Id { get; set; }
		public DateTime DateTime { get; set; }

		public double Production { get; set; }

		public double Consumption { get; set; }
		public string ConsumerType { get; set; }

		public long ConsumerId { get; set; }
	}
}