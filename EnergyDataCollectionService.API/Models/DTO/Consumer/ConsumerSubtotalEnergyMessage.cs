﻿namespace EnergyDataCollectionService.Models.DTO
{
	public class ConsumerSubtotalEnergyMessage
	{
		public long TotalConsumption { get; set; }
		public long TotalProduction { get; set; }
		public long ConsumerCount { get; set; }
		public string ConsumerType { get; set; }
	}
}