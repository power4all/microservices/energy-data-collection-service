﻿namespace EnergyDataCollectionService.Models.DTO
{
	public class ProducerAttributeDTO
	{
		public string AttributeName { get; set; }
		public string Value { get; set; }
	}
}