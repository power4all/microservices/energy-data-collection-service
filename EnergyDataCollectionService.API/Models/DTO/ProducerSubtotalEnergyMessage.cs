﻿namespace EnergyDataCollectionService.Models.DTO
{
	public class ProducerSubtotalEnergyMessage
	{
		public long TotalOutput { get; set; }
		public long ProducerCount { get; set; }
		public string ProducerType { get; set; }
	}
}