﻿using EnergyDataCollectionService.Models.Entity.Consumers;
using EnergyDataCollectionService.Models.Entity.Producers;
using EnergyDataCollectionService.Models.Entity.Solar.Message;
using EnergyDataCollectionService.Models.Entity.Wind.Message;
using Microsoft.EntityFrameworkCore;

namespace EnergyDataCollectionService.Data
{
	public class ApplicationDbContext : DbContext
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
		{
		}

		public DbSet<SolarData> SolarData { get; set; }
		public DbSet<WindData> WindData { get; set; }
		public DbSet<Producer> Producers { get; set; }
		public DbSet<Consumer> Consumers { get; set; }
		public DbSet<ConsumerMessage> ConsumerMessages { get; set; }
		public DbSet<ProducerMessage> ProducerMessages { get; set; }
		public DbSet<ProducerType> ProducerTypes { get; set; }
		public DbSet<ConsumerType> ConsumerTypes { get; set; }
	}
}