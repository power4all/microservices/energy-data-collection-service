﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace EnergyDataCollectionService.Migrations
{
	public partial class initialcreate : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.CreateTable(
				name: "ConsumerTypes",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					Type = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_ConsumerTypes", x => x.Id);
				});

			migrationBuilder.CreateTable(
				name: "ProducerTypes",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					Type = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_ProducerTypes", x => x.Id);
				});

			migrationBuilder.CreateTable(
				name: "SolarData",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					GHI = table.Column<double>(type: "double", nullable: false),
					Date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_SolarData", x => x.Id);
				});

			migrationBuilder.CreateTable(
				name: "WindData",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					WindSpeed = table.Column<double>(type: "double", nullable: false),
					Temprature = table.Column<double>(type: "double", nullable: false),
					Date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_WindData", x => x.Id);
				});

			migrationBuilder.CreateTable(
				name: "Consumers",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					ConsumerName = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
					ConsumerTypeId = table.Column<long>(type: "bigint", nullable: false),
					isProducing = table.Column<bool>(type: "tinyint(1)", nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_Consumers", x => x.Id);
					table.ForeignKey(
						name: "FK_Consumers_ConsumerTypes_ConsumerTypeId",
						column: x => x.ConsumerTypeId,
						principalTable: "ConsumerTypes",
						principalColumn: "Id",
						onDelete: ReferentialAction.Cascade);
				});

			migrationBuilder.CreateTable(
				name: "Producers",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					ProducerName = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
					Longitude = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
					Latitude = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
					ProducerTypeId = table.Column<long>(type: "bigint", nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_Producers", x => x.Id);
					table.ForeignKey(
						name: "FK_Producers_ProducerTypes_ProducerTypeId",
						column: x => x.ProducerTypeId,
						principalTable: "ProducerTypes",
						principalColumn: "Id",
						onDelete: ReferentialAction.Cascade);
				});

			migrationBuilder.CreateTable(
				name: "ConsumerAttribute",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					AttributeName = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
					Value = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
					ConsumerId = table.Column<long>(type: "bigint", nullable: true)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_ConsumerAttribute", x => x.Id);
					table.ForeignKey(
						name: "FK_ConsumerAttribute_Consumers_ConsumerId",
						column: x => x.ConsumerId,
						principalTable: "Consumers",
						principalColumn: "Id",
						onDelete: ReferentialAction.Restrict);
				});

			migrationBuilder.CreateTable(
				name: "ConsumerMessages",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					DateTime = table.Column<DateTime>(type: "datetime(6)", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
					Production = table.Column<double>(type: "double", nullable: false),
					Consumption = table.Column<double>(type: "double", nullable: false),
					ConsumerType = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
					ConsumerId = table.Column<long>(type: "bigint", nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_ConsumerMessages", x => x.Id);
					table.ForeignKey(
						name: "FK_ConsumerMessages_Consumers_ConsumerId",
						column: x => x.ConsumerId,
						principalTable: "Consumers",
						principalColumn: "Id",
						onDelete: ReferentialAction.Cascade);
				});

			migrationBuilder.CreateTable(
				name: "ProducerAttribute",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					AttributeName = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
					Value = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
					ProducerId = table.Column<long>(type: "bigint", nullable: true)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_ProducerAttribute", x => x.Id);
					table.ForeignKey(
						name: "FK_ProducerAttribute_Producers_ProducerId",
						column: x => x.ProducerId,
						principalTable: "Producers",
						principalColumn: "Id",
						onDelete: ReferentialAction.Restrict);
				});

			migrationBuilder.CreateTable(
				name: "ProducerMessages",
				columns: table => new
				{
					Id = table.Column<long>(type: "bigint", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
					DateTime = table.Column<DateTime>(type: "datetime(6)", nullable: false)
						.Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.ComputedColumn),
					Output = table.Column<double>(type: "double", nullable: false),
					ProducerType = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true),
					ProducerId = table.Column<long>(type: "bigint", nullable: false)
				},
				constraints: table =>
				{
					table.PrimaryKey("PK_ProducerMessages", x => x.Id);
					table.ForeignKey(
						name: "FK_ProducerMessages_Producers_ProducerId",
						column: x => x.ProducerId,
						principalTable: "Producers",
						principalColumn: "Id",
						onDelete: ReferentialAction.Cascade);
				});

			migrationBuilder.CreateIndex(
				name: "IX_ConsumerAttribute_ConsumerId",
				table: "ConsumerAttribute",
				column: "ConsumerId");

			migrationBuilder.CreateIndex(
				name: "IX_ConsumerMessages_ConsumerId",
				table: "ConsumerMessages",
				column: "ConsumerId");

			migrationBuilder.CreateIndex(
				name: "IX_Consumers_ConsumerTypeId",
				table: "Consumers",
				column: "ConsumerTypeId");

			migrationBuilder.CreateIndex(
				name: "IX_ProducerAttribute_ProducerId",
				table: "ProducerAttribute",
				column: "ProducerId");

			migrationBuilder.CreateIndex(
				name: "IX_ProducerMessages_ProducerId",
				table: "ProducerMessages",
				column: "ProducerId");

			migrationBuilder.CreateIndex(
				name: "IX_Producers_ProducerTypeId",
				table: "Producers",
				column: "ProducerTypeId");
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropTable(
				name: "ConsumerAttribute");

			migrationBuilder.DropTable(
				name: "ConsumerMessages");

			migrationBuilder.DropTable(
				name: "ProducerAttribute");

			migrationBuilder.DropTable(
				name: "ProducerMessages");

			migrationBuilder.DropTable(
				name: "SolarData");

			migrationBuilder.DropTable(
				name: "WindData");

			migrationBuilder.DropTable(
				name: "Consumers");

			migrationBuilder.DropTable(
				name: "Producers");

			migrationBuilder.DropTable(
				name: "ConsumerTypes");

			migrationBuilder.DropTable(
				name: "ProducerTypes");
		}
	}
}