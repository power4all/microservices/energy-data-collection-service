﻿// <auto-generated />
using System;
using EnergyDataCollectionService.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace EnergyDataCollectionService.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 64)
                .HasAnnotation("ProductVersion", "5.0.4");

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Consumers.Consumer", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<string>("ConsumerName")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<long>("ConsumerTypeId")
                        .HasColumnType("bigint");

                    b.Property<bool>("isProducing")
                        .HasColumnType("tinyint(1)");

                    b.HasKey("Id");

                    b.HasIndex("ConsumerTypeId");

                    b.ToTable("Consumers");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Consumers.ConsumerAttribute", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<string>("AttributeName")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<long?>("ConsumerId")
                        .HasColumnType("bigint");

                    b.Property<string>("Value")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.HasKey("Id");

                    b.HasIndex("ConsumerId");

                    b.ToTable("ConsumerAttribute");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Consumers.ConsumerMessage", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<long>("ConsumerId")
                        .HasColumnType("bigint");

                    b.Property<string>("ConsumerType")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<double>("Consumption")
                        .HasColumnType("double");

                    b.Property<DateTime>("DateTime")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasColumnType("datetime(6)");

                    b.Property<double>("Production")
                        .HasColumnType("double");

                    b.HasKey("Id");

                    b.HasIndex("ConsumerId");

                    b.ToTable("ConsumerMessages");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Consumers.ConsumerType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<string>("Type")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.HasKey("Id");

                    b.ToTable("ConsumerTypes");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Producers.Producer", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<string>("Latitude")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Longitude")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("ProducerName")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<long>("ProducerTypeId")
                        .HasColumnType("bigint");

                    b.HasKey("Id");

                    b.HasIndex("ProducerTypeId");

                    b.ToTable("Producers");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Producers.ProducerAttribute", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<string>("AttributeName")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<long?>("ProducerId")
                        .HasColumnType("bigint");

                    b.Property<string>("Value")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.HasKey("Id");

                    b.HasIndex("ProducerId");

                    b.ToTable("ProducerAttribute");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Producers.ProducerMessage", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<DateTime>("DateTime")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasColumnType("datetime(6)");

                    b.Property<double>("Output")
                        .HasColumnType("double");

                    b.Property<long>("ProducerId")
                        .HasColumnType("bigint");

                    b.Property<string>("ProducerType")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.HasKey("Id");

                    b.HasIndex("ProducerId");

                    b.ToTable("ProducerMessages");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Producers.ProducerType", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<string>("Type")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.HasKey("Id");

                    b.ToTable("ProducerTypes");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Solar.Message.SolarData", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime(6)");

                    b.Property<double>("GHI")
                        .HasColumnType("double");

                    b.HasKey("Id");

                    b.ToTable("SolarData");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Wind.Message.WindData", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime(6)");

                    b.Property<double>("Temprature")
                        .HasColumnType("double");

                    b.Property<double>("WindSpeed")
                        .HasColumnType("double");

                    b.HasKey("Id");

                    b.ToTable("WindData");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Consumers.Consumer", b =>
                {
                    b.HasOne("EnergyDataCollectionService.Models.Entity.Consumers.ConsumerType", "ConsumerType")
                        .WithMany()
                        .HasForeignKey("ConsumerTypeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("ConsumerType");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Consumers.ConsumerAttribute", b =>
                {
                    b.HasOne("EnergyDataCollectionService.Models.Entity.Consumers.Consumer", null)
                        .WithMany("Attributes")
                        .HasForeignKey("ConsumerId");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Consumers.ConsumerMessage", b =>
                {
                    b.HasOne("EnergyDataCollectionService.Models.Entity.Consumers.Consumer", "Consumer")
                        .WithMany()
                        .HasForeignKey("ConsumerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Consumer");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Producers.Producer", b =>
                {
                    b.HasOne("EnergyDataCollectionService.Models.Entity.Producers.ProducerType", "ProducerType")
                        .WithMany()
                        .HasForeignKey("ProducerTypeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("ProducerType");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Producers.ProducerAttribute", b =>
                {
                    b.HasOne("EnergyDataCollectionService.Models.Entity.Producers.Producer", null)
                        .WithMany("Attributes")
                        .HasForeignKey("ProducerId");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Producers.ProducerMessage", b =>
                {
                    b.HasOne("EnergyDataCollectionService.Models.Entity.Producers.Producer", "Producer")
                        .WithMany()
                        .HasForeignKey("ProducerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Producer");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Consumers.Consumer", b =>
                {
                    b.Navigation("Attributes");
                });

            modelBuilder.Entity("EnergyDataCollectionService.Models.Entity.Producers.Producer", b =>
                {
                    b.Navigation("Attributes");
                });
#pragma warning restore 612, 618
        }
    }
}
