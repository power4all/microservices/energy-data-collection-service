using EnergyDataCollectionService.Data;
using EnergyDataCollectionService.Helpers;
using EnergyDataCollectionService.Logic.Consumers;
using EnergyDataCollectionService.Logic.Managers;
using EnergyDataCollectionService.Logic.Producers;
using EnergyDataCollectionService.Logic.Services;
using EnergyDataCollectionService.RabbitMQEventbus;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using RabbitMQEventbus.Extension.DependencyInjection;
using RabbitMQEventbus.Extension.FunctionProvider;
using RabbitMQEventbus.Extension.RabbitMQEventbusConfiguration;
using System;
using System.Net.Http;

namespace EnergyDataCollectionService
{
	public class Startup
	{
		private readonly IWebHostEnvironment _env;

		public Startup(IConfiguration configuration, IWebHostEnvironment env)
		{
			_env = env;
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<ApplicationDbContext>(options =>
			{
				options.UseMySql(
					Configuration.GetConnectionString("MySql"),
					new MySqlServerVersion(new Version(8, 0, 23)),
				mySqlOptions =>
				{
					mySqlOptions.EnableRetryOnFailure(
						maxRetryCount: 2);
				});
			});

			services.AddCors(options =>
			{
				options.AddDefaultPolicy(
					builder =>
					{
						builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
					});
			});

			services.Configure<RouteOptions>(o => o.LowercaseUrls = true);
			services.AddControllers(o =>
			{
				o.Conventions.Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
			});

			services.AddHttpClient<HttpClient>();

			// services.AddTransient<ISearch, Search>();

			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo { Title = "EnergyDataCollectionService.API", Version = "v1" });
			});

			services.AddSingleton<WeatherService>();
			services.Configure<RabbitMQEventbusConfiguration>(Configuration);
			services.AddOptions<RabbitMQEventbusConfiguration>();
			services.AddSingleton<IRabbitMQFunctionProvider>(s =>
			{
				return new FunctionProvider(
					s.GetService<WeatherService>(),
					s.GetService<ILogger<FunctionProvider>>(),
					s.GetService<IServiceScopeFactory>()
					);
			});
			services.AddRabbitMQEventbus(_env);
			services.AddScoped<InitProducers>();
			services.AddTransient<ProducerService>();
			services.AddSingleton<ProducerManager>();

			services.AddScoped<InitConsumers>();
			services.AddTransient<ConsumerService>();
			services.AddSingleton<ConsumerManager>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				app.UseSwagger();
				app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "EnergyDataCollectionService.API v1"));
				// SeedDb();
			}

			app.UseRabbitMQEventBus();

			app.UseRouting();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});

			var initProducers = serviceProvider.GetService<InitProducers>();
			var initConsumers = serviceProvider.GetService<InitConsumers>();

			initProducers?.LogStarted();
			initConsumers?.LogStarted();
		}
	}
}